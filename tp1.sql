-- Programmeur :
-- 0. Crée les tables

drop table client cascade constraints;
drop table village cascade constraints;
drop table sejour cascade constraints;

drop sequence seq_client;
drop sequence seq_village;
drop sequence seq_sejour;

create sequence seq_client start with 1;
create sequence seq_village start with 10;
create sequence seq_sejour start with 100;

create table client (
    idc int primary key,
    nom varchar2(10) not null,
    age int not null, check( age <= 120 and age >= 0),
    avoir int default 2000 not null, check (avoir <= 2000 and avoir >= 0)
);

create table village (
    idv int primary key,
    ville varchar2(20) not null,
    activite varchar2(20),
    prix int not null, check( prix > 0 and prix <= 2000),
    capacite int not null, check(capacite > 0 and capacite <= 1000)
);

create table sejour (
    ids int primary key,
    idc int not null, foreign key (idc) references client,
    idv int not null, foreign key (idv) references village,
    jour int not null, check (jour >= 1 and jour <= 365),
    unique (idc,jour)
);


-- Contrainte NON SQL
-- 1. Pour chaque client, son avoir + le prix du village dans chacun de ses sejours doit etre
-- égal à 2000
-- 2. pour chaque sejour, si ils sont dans la meme ville et jour, leur nombre ne doit
-- pas excder la capacite du village associé

-- Exemples
insert into client
values (seq_client.nextval,'Riton',23,1700);

insert into village 
values (seq_village.nextval,'Rio','kitesurf',50,250);

insert into sejour
values (seq_sejour.nextval,1,10,265);

-- Employé
-- 1. Créer un village 

insert into village
values (seq_village.nextval,'Lille','pluie',10,1000);
insert into village
values (seq_village.nextval,'Lille','frites',5,10);
insert into village
values (seq_village.nextval,'Los Angeles','films',1500,50);

-- Modele : 
-- create_village(idv,v,a,p,c) :
--     insert into village
--         values(idv,v,a,p,c)
-- retour : rien

-- 2. Consulter villages
select * from village;

-- retour : idv, ville, activite, prix, capacite

-- 3. Modifier village (seulement activite)
update village
   set activite='inondation'
 where idv = 12;

-- Modele
-- update_village(l_idv,a):
--     update village
--         set activite = a
--     where idv = l_idv

-- 4. Consulter sejour
select * from sejour;

-- 5. Traitement 3
select count(*)
  from sejour
 where jour <= 300;
delete from sejour
 where jour <= 300;

--  Modele : 
-- traitement3(le_jour): 
-- select count(*) from sejour where jour <= le_jour
-- delete from sejour where jour <= le_jour

-- Acteur : client
-- 6. Traitement 1

insert into client
values (seq_client.nextval,'Jeanne',30,2000);


insert into client
values (seq_client.nextval,'Jean',19,2000);

-- Modele :
-- traitement1(le_nom,l_age):
--     insert into client 
--     values (4,le_nom,l_age,2000)

-- 7. Traitement2
select idv,prix,activite
  from village
  order by prix desc;
insert into sejour 
  values (seq_sejour.nextval,3,12,360);
update client
   set avoir=500
 where idc = 3;

-- 8. Consulter village qui n'ont 0 sejour

select idv,ville,activite,prix
    from village
    where idv not in (
        select idv
          from sejour
    );

-- 9. Consulter ligne avec son IDC
select * from client
    where idc = 3
    and nom = 'Jean';

select ids,client.idc,idv,jour
    from client,sejour
    -- on fait le lien entre client et sejour
    where client.idc = sejour.idc
    and client.idc = 3
    and client.nom = 'Jean';

select village.idv, ville, activite, prix, capacite from
    sejour,client,village
    -- lien entre client, sejour et village
    where client.idc = sejour.idc
    and sejour.idv = village.idv
    -- on check la bonne personne
    and client.idc = 3
    and client.nom = 'Jean';

set serveroutput on;

-- procedure traitement3
create or replace procedure traitement3 (
  le_jour in sejour.jour%type,
  le_nb out int
)
is
begin
  select count(*) into le_nb from sejour where jour <= le_jour;
  delete from sejour where jour <= le_jour;
end;
/

declare
  x int;
begin
  traitement3(200,x);
  dbms_output.put_line('x : ' || x);
end;
/

-- fonction traitement 1
create or replace function traitement1 (
  le_nom in client.nom%type,
  l_age in client.age%type
) return client.idc%type
is
l_idc client.idc%type;
begin
  insert into client
  values (seq_client.nextval,le_nom,l_age,2000)
  returning idc into l_idc;
  return l_idc;
end;
/

-- Consulter informations
create or replace procedure consulter_info (
  l_idc in client.idc%type,
  le_nom in client.nom%type
)
is
cursor c1 is
select ids,village.idv,jour,ville,activite,prix
  from sejour,village,client
  where client.idc = l_idc
  and client.nom = le_nom
  and village.idv = sejour.idv
  and client.idc = sejour.idc;
cursor c2 is
  select * from client
    where idc = l_idc
    and nom = le_nom;
le_client client%rowtype;
begin
  for x in c1 loop
    dbms_output.put_line('sejour :' || x.ids || ' ' || x.idv || '(' || x.ville || ', ' || x.activite || ', ' || x.prix || 'e) ' || x.jour);
  end loop;

  open c2;
  fetch c2 into le_client;
  while c2%found loop
   dbms_output.put_line('client :' || le_client.nom || ' ' || le_client.age);  
    fetch c2 into le_client;
  end loop;
  close c2;
end;
/

exec consulter_info(3,'Jean');

create or replace procedure traitement2 (
  l_idc in client.idc%type,
  la_ville in village.ville%type,
  le_jour in sejour.jour%type,
  l_idv out village.idv%type,
  l_ids out sejour.ids%type,
  l_activite out village.activite%type
)
is
  le_prix village.prix%type;
  cursor c1 is 
  select idv,activite,prix
    from village
    where ville = la_ville
    order by prix desc;
begin
  open c1;
  fetch c1 into l_idv,l_activite,le_prix;
  if c1%notfound then
    l_idv := -1;    
    l_ids := -1;    
    l_activite := 'neant';    
  else
    -- on decremente l'avoir
    update client
      set avoir=avoir - le_prix
    where idc = l_idc;
    -- on rajoute le sejour
    insert into sejour
    values (seq_sejour.nextval,l_idc,l_idv,le_jour)
    returning ids into l_ids;
  end if;
  close c1;
end;
/

declare
  l_idv village.idv%type;
  l_ids sejour.ids%type;
  l_activite village.activite%type;
begin
  traitement2(2,'Lille',30,l_idv,l_ids,l_activite);
  dbms_output.put_line(l_idv || ' ' || l_ids || ' ' || l_activite);
end;
/

declare
  l_age client.age%type;
begin
  update client
    set nom = 'Jacki'
    where idc = 1
  returning age into l_age;
  dbms_output.put_line('age : ' || l_age);
end;
/